<section class="pricing-table section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-top text-center">
                    <h2>Veja algumas vantagens de se cadastrar</h2>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="single-table text-center mb-4 mb-md-0">
                    <div class="table-top">
                        <h3>Atendimento Personalizado</h3>
                        <i class="fa fa-home"></i>
                    </div>
                    <ul class="my-5">
                        <li class="mb-2" style="padding: 30px">O sistema faz o intermédio entre o profissional e o cliente, poderão combinar como será o atendimento. Em sua casa, na casa do
                            cliente. Da maneira que ficar melhor.</li>                           
                    </ul>
                    <a href="<?= base_url() ?>criarcontasite" class="template-btn">Cadastre-se</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-table text-center mb-4 mb-md-0">
                    <div class="table-top">
                        <h3>Controle Sobre Contatos</h3>
                        <i class="fa fa-envelope"></i>
                    </div>
                    <ul class="my-5">
                        <li class="mb-2" style="padding: 30px">Através de seu painel de controle será possível visualizar as mensagens recebidas, filtrar e responder seus clientes rapidamente. Isso de uma forma
                            simples e dinâmica.</li>

                    </ul>
                    <a href="<?= base_url() ?>criarcontasite" class="template-btn">Cadastre-se</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-table text-center">
                    <div class="table-top">
                        <h3>Fique visivel para todos</h3>
                        <i class="fa fa-user-circle"></i>
                    </div>
                    <ul class="my-5">
                        <li class="mb-2" style="padding: 30px">Com seu pefil em funcionamento, seu trabalho ficará visivel para todos os possíveis clientes que buscarem pelos tipos de serviços 
                            que pode oferer no portal.</li>

                    </ul>
                    <a href="<?= base_url() ?>criarcontasite" class="template-btn">Cadastre-se</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Pricing Table End -->
<!-- Pricing Box Starts -->
<div class="pricing-box section-padding3">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="single-box2 text-center">
                    <h3>Teste por 1 mês Gratuito</h3>
                    <p>Estamos disponibilizando um mês de teste gratuito da plataforma, seu anuncio ficara exposto sem nenhuma cobrança por esse período, começa agora.</p>
                    <a href="<?= base_url() ?>criarcontasite" class="template-btn">Cadastre-se</a>                    
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
<div class="pricing-box section-padding3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="single-box1 text-center mb-4 mb-md-0">
                    <h3>Acessível para todos</h3>
                    <p>Após o período de teste verifique qual plano ficará melhor para você. Temos planos a partir de R$ 29,99. Aproveite e divulgue seu trabalho.</p>                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="single-box2 text-center">
                    <h3>Cadastro Gratuito</h3>
                    <p>Você não pagará pelo cadastro no portal. A mensalidade será cobrada no momento que optar por exibir o anuncio de seu trabalho no site.</p>                    
                </div>
            </div>
        </div>
    </div>
</div>
