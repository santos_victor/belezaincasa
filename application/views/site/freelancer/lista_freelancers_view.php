<?php
header('Cache-Control: no cache');
session_cache_limiter('private_no_expire');
?>
<!-- Header Area Starts -->
<header class="header-area single-page">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">                           
                </div>
                <div class="col-lg-10">

                </div>
            </div>
        </div>
    </div>
    <div class="page-title text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <h2>Job Search</h2>
                    <p>There spirit beginning bearing the open at own every give appear in third you sawe two boys</p>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header Area End -->


<!-- Search Area Starts -->
<div class="search-area">
    <div class="search-area">
        <div class="search-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="<?= base_url() ?>buscarfreelancer" class="d-md-flex justify-content-between" method="POST">
                            <select name="area">
                                <option disabled="" selected="" value="">Categorias</option>
                                <?php foreach ($categorias as $categoria): ?>
                                    <option value="<?= $categoria['categoria_id'] ?>"><?= $categoria['categoria_nome'] ?></option>
                                <?php endforeach; ?>
                            </select>                       
                            <select name="estado">
                                <option disabled= selected value="">Estado</option>                           
                                <option value="7">Brasília</option>                           
                            </select>
                            <select name="cidade">
                                <option disabled selected value="">Cidade</option>                           
                                <?php foreach ($brasiliacidades as $brasiliacidade): ?>
                                    <option value="<?= $brasiliacidade['cidadedf_id'] ?>"><?= $brasiliacidade['cidadedf_nome'] ?></option>
                                <?php endforeach; ?>                      
                            </select>
                            <button type="submit" class="template-btn">Buscar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <span>49 Results found for “Telecommunication”</span>
            </div>
        </div>
    </div>
</div>
<!-- Search Area End -->


<!-- Jobs Area Starts -->
<section class="jobs-area section-padding">
    <div class="container">
        <div class="row">
            <?php
            if (!empty($freelancers)) {
                foreach ($freelancers as $freelancer):
                    ?>
                    <div class="col-lg-12">
                        <div class="single-job mb-4 d-lg-flex justify-content-between">
                            <div class="job-text">
                                <h4><?= $freelancer['freelancer_nome'] ?></h4>
                                <ul class="mt-4">
                                    <li class="mb-3"><h5><i class="fa fa-star"></i><?= $freelancer['categoria_nome'] ?></h5></li>
                                    <li class="mb-3"><h5><i class="fa fa-map-marker"></i><?= $freelancer['estado_nome'] ?></h5></li>
                                    <li><h5><i class="fa fa-map"></i> <?= $freelancer['cidadedf_nome'] ?></h5></li>
                                    <li><h5><i class="fa fa-phone"></i> <?= $freelancer['freelancer_celular'] ?></h5></li>
                                </ul>
                            </div>
                            <div class="job-img align-self-center">
                                <img width="100" class="img-fluid" src="<?= base_url() ?>public/images/freelas/<?= $freelancer['freelancer_foto'] ?>">
                            </div>
                            <div class="job-btn align-self-center">
                                <a href="<?= base_url() ?>perfilfreelancer/<?= $freelancer['freelancer_id'] ?>" class="third-btn job-btn1">Ver Perfil</a>                            
                            </div>
                        </div>

                    </div>
                    <?php
                endforeach;
            }else {
                echo '<p class="col-lg-12 alert alert-danger text-center">Nunhum resultado encontrado!</p>';
            }
            ?>
        </div>
        <div class="more-job-btn mt-5 text-center">
            <a href="#" class="template-btn">more job post</a>
        </div>
    </div>
</section>
<!-- Jobs Area End -->



<!-- Footer Area Starts -->

