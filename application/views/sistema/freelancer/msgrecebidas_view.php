<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Mensagens Recebidas</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Mensagens</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"></strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Telefone</th>
                                    <th>Descrição</th>
                                    <th>Exibir</th>
                                    <th>Responder</th>                                   
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php foreach ($mensagens as $mensagem):?>
                                <?php if($mensagem['mens_respondida'] == '0'){ ?>
                                <tr>
                                    <td><?= $mensagem['mens_nome_contato']?></td>
                                    <td><?= $mensagem['mens_email_contato']?></td>
                                    <td><?= $mensagem['mens_tel_contato']?></td>
                                    <td><?= substr($mensagem['mens_mensagem_contato'], 0, 30)?></td>
                                    <td><a href="#" class="btn btn-primary btn-sm">Exibir</a></td>
                                    <td><a href="<?= base_url()?>respondermensagem/<?=$mensagem['mens_id']?>" class="btn btn-dark btn-sm">Responder</a></td>                                   
                                </tr>
                                <?php } ?>
                                <?php endforeach;?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->