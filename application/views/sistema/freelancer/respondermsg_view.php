<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Formulário</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><strong>Responder Mensagem</strong></div>
                <p style="margin-left: 20px"><strong>Mensagem Recebida:</strong> <?=$mensagem['mens_mensagem_contato']?></p>
                <form class="form" action="<?= base_url()?>respondemensagemcliente" method="POST">
                    <input name="id" type="hidden" value="<?=$mensagem['mens_id']?>">
                    <div class="card-body card-block">
                        <p class="text-capitalize text-center"><?= $this->session->flashdata("danger"); ?></p>
                        <p class="text-capitalize text-center alert-success"><?= $this->session->flashdata("success"); ?></p>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nome" class="form-control-label">Nome</label>
                                <input type="text" id="nome1" class="form-control" name="nome" value="<?=$mensagem['mens_nome_contato']?>">
                            </div> 
                             <div class="form-group col-md-4">
                                <label for="email" class="form-control-label">Para</label>
                                <input type="text" id="email1" class="form-control" name="email" value="<?=$mensagem['mens_email_contato']?>">
                            </div>  
                        </div>                        
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Mensagem</label>
                                    <textarea type="text" name="mensagem" class="form-control" rows="10" cols="30" id="txtArtigo"></textarea>
                                </div>
                            </div>
                        </div>           
                        <input type="submit" class="btn btn-primary" value="Enviar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>