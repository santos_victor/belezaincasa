<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Mensagens Respondidas</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Mensagens Respondidas</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"></strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Telefone</th>
                                    <th>Descrição</th>
                                    <th>Exibir</th>                                    
                                    <th>Excluir</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php foreach ($mensagens as $mensagem):?>
                                <?php if($mensagem['mens_respondida'] == '1'){ ?>
                                <tr>
                                    <td><?= $mensagem['mens_nome_contato']?></td>
                                    <td><?= $mensagem['mens_email_contato']?></td>
                                    <td><?= $mensagem['mens_tel_contato']?></td>
                                    <td><?= substr($mensagem['mens_mensagem_contato'], 0, 30)?></td>
                                    <td><a href="#" class="btn btn-primary btn-sm">Exibir</a></td> 
                                    <td><button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">Excluir</button></td>                                   
                                </tr>
                                <?php } ?>
                                <?php endforeach;?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exluir Mensagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <p style="margin-left: 15px">Deseja exluir essa mensagem?</p>
            <div class="modal-body">
                <td><a href="<?= base_url()?>excluirmensagem/<?=$mensagem['mens_id']?>" class="btn btn-danger btn-sm">Excluir</a></td>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>