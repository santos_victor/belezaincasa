<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Formulário</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">E-mail</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><strong>Dados do Anuncio</strong></div>
                <form class="form" action="<?= base_url() ?>enviaremailsuporte" method="POST">
                    <div class="card-body card-block">
                        <p class="text-capitalize text-center"><?= $this->session->flashdata("danger"); ?></p>
                        <?php if (($mensagem = $this->session->flashdata("success"))) { ?>
                            <center><div class="alert alert-success"><?= $mensagem ?></div></center> 
                        <?php } ?> 
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nome" class="form-control-label">Nome Usuário</label>
                                <input type="text" id="titulo" class="form-control" name="nome" value="<?= usuario('freelancer_nome') ?>">
                            </div>                         
                        </div> 
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nome" class="form-control-label">Assunto</label>
                                <input type="text" id="titulo" class="form-control" name="assunto">
                            </div>                         
                        </div>                        
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Mensagem</label>
                                    <textarea type="text" name="mensagem" class="form-control" rows="10" cols="30" maxlength="20" id="txtArtigo" ></textarea>
                                </div>
                            </div>
                        </div>           
                        <input type="submit" class="btn btn-primary" value="Enviar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>