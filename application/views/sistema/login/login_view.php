<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-logo">
                 <a href="<?= base_url() ?>Site/telaInicial"><img src="<?= base_url() ?>public/site/belezaincasa_logo_novo.png" alt="logo"></a>
            </div>
            <div class="login-form">
                <form class="form" action="<?= base_url()?>autenticarsistema" method="POST">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" placeholder="Email" name="login">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Password" name="senha">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                        <label class="pull-right">
                            <a href="#">Esqueceu a senha?</a>
                        </label>

                    </div>
                    <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Login</button>
                    <div class="social-login-content">
                        <div class="social-button">
                            <button type="button" class="btn social google-plus btn-flat btn-addon mb-3"><i class="ti-google"></i>Entre com o Google</button>                            
                        </div>
                    </div>
                    <div class="register-link m-t-15 text-center">
                        <p>Não tem conta? <a href="<?= base_url()?>criarcontasite"> Crie sua conta aqui</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



