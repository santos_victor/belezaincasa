<div style="margin-top: 70px">
    <div class="container">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="login-logo">
                 <a href="<?= base_url() ?>Site/telaInicial"><img src="<?= base_url() ?>public/site/belezaincasa_logo_novo.png" alt="logo"></a>
            </div>
            <div class="card">
                <div class="card-header">Cadastro</div>
                <div class="card-body card-block">
                    <form action="<?= base_url()?>cadastrarusuariosite" method="post" class="form">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input type="text" id="username" name="nome" placeholder="Nome Completo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input type="email" id="email" name="email" placeholder="Email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                <input type="password" id="password" name="senha" placeholder="Senha" class="form-control">
                            </div>
                        </div>                         
                        <div class="">
                        <button type="submit" class="btn btn-success btn-sm">Cadastrar</button>
                        <a href="<?= base_url()?>telalogin" type="submit" class="btn btn-danger btn-sm">Voltar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>