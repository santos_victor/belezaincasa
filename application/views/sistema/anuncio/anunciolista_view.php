<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Formulário</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Table</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Data de Inclusão</th>
                                    <th>Hora de Inclusão</th>
                                    <th>Descrição</th>
                                    <th>Publicar</th>
                                    <th>Alterar</th>
                                    <th>Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($anuncios as $anuncio):?>
                                <tr>
                                    <td><?= $anuncio['anuncio_titulo']?></td>
                                    <td><?= $anuncio['anuncio_data']?></td>
                                    <td><?= $anuncio['anuncio_hora']?></td>
                                    <td><?= substr($anuncio['anuncio_desc'], 0, 30)?></td>
                                    <td><a href="<?= base_url()?>anunciopagamento" class="btn btn-success btn-sm">Publicar</a></td>
                                    <td><a href="<?= base_url()?>cadastroanuncio?id=<?=$anuncio['anuncio_id']?>" class="btn btn-dark btn-sm">Alterar</a></td>
                                    <td><a href="<?= base_url()?>excluiranuncio/<?=$anuncio['anuncio_id']?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">Excluir</a></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exluir Mensagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <p style="margin-left: 15px">Deseja exluir seu anuncio?</p>
            <div class="modal-body">
                <td><a href="<?= base_url()?>excluiranuncio/<?=$anuncio['anuncio_id']?>" class="btn btn-danger btn-sm">Excluir</a></td>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>