<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Formulário</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><strong>Dados do Anuncio</strong></div>
                <form class="form" action="<?= base_url() ?><?= (isset($anuncio['anuncio_id']) ? 'anuncioaltera/' . $anuncio['anuncio_id'] : 'cadastraranuncio') ?>" method="POST">
                    <div class="card-body card-block">
                        <p class="text-capitalize text-center"><?= $this->session->flashdata("danger"); ?></p>
                        <p class="text-capitalize text-center alert-success"><?= $this->session->flashdata("success"); ?></p>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nome" class="form-control-label">Título</label>
                                <input type="text" id="titulo" class="form-control" name="titulo" value="<?= (isset($anuncio['anuncio_titulo']) || isset($_GET['id']) ? $anuncio['anuncio_titulo'] : '') ?>">
                            </div>                         
                        </div>                        
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea type="text" name="descricao" class="form-control" rows="10" cols="30" maxlength="20" id="txtArtigo" >         
                                        <?= (isset($anuncio['anuncio_desc']) || isset($_GET['id']) ? $anuncio['anuncio_desc'] : '') ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>           
                        <input type="submit" class="btn btn-primary" value="<?= (isset($anuncio['anuncio_id']) ? 'Alterar' : 'Cadastrar') ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>