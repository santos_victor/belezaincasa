<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
<script src="<?= base_url() ?>public/vendors/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url() ?>public/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="<?= base_url() ?>public/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>public/assets/js/main.js"></script>
<script src="<?= base_url() ?>public/vendors/chart.js/dist/Chart.bundle.min.js"></script>
<script src="<?= base_url() ?>public/assets/js/dashboard.js"></script>
<script src="<?= base_url() ?>public/assets/js/widgets.js"></script>
<script src="<?= base_url() ?>public/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?= base_url() ?>public/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<script src="<?= base_url() ?>public/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?= base_url() ?>public/ckeditor/ckeditor.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>public/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?= base_url() ?>public/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?= base_url() ?>public/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url() ?>public/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>public/assets/js/init-scripts/data-table/datatables-init.js"></script>
<script>
    CKEDITOR.replace('txtArtigo');
</script>
<script>
    (function ($) {
        "use strict";

        jQuery('#vmap').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#ffffff',
            hoverOpacity: 0.7,
            selectedColor: '#1de9b6',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#1de9b6', '#03a9f5'],
            normalizeFunction: 'polynomial'
        });
    })(jQuery);
</script>

<script type="text/javascript">

    function preview_images()
    {
        var total_file = document.getElementById("images").files.length;
        for (var i = 0; i < total_file; i++)
        {
            $('#image_preview').append("<div class='col-md-6' style='padding-bottom: 10px;'><img class='img-fluid' width='460' height='345' src='" + URL.createObjectURL(event.target.files[i]) + "'></div>");

        }
    }

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#bootstrap-data-table-export').DataTable();
    });
</script>
</body>
</html>