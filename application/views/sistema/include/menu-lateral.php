<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./">BELEZA IN CASA</a>
            <!--<a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>-->
        </div>
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="<?= base_url() ?>home"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    <a href="<?= base_url() ?>Site/telaInicial" target="_blank"> <i class="menu-icon fa fa-arrow-circle-o-right"></i>Ir Para o Site </a>
                    <a href="<?= base_url() ?>perfilfreelancer/<?= usuario('freelancer_id')?>" target="_blank"> <i class="menu-icon fa fa-user-o"></i>Visualizar Meu Perfil </a>
                </li>
                <h3 class="menu-title">Controle</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Dados</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-user-circle-o"></i><a href="<?= base_url() ?>cadastrousuario">Dados de Usuário</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-envelope-square"></i>Mensagens</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-envelope-o"></i><a href="<?= base_url() ?>mensagensrecebidas">Mensagens Recebidas</a></li>
                        <li><i class="fa fa-envelope-open-o"></i><a href="<?= base_url() ?>mensagensrespondidas">Mensagens Respondidas</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-comment-o"></i>Anúncio</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-angle-double-up"></i><a href="<?= base_url() ?>cadastroanuncio">Criar Anúncio</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="<?= base_url() ?>anunciolista/<?= usuario('freelancer_id') ?>">Verificar Anúncio</a></li>
                    </ul>
                </li>
                <h3 class="menu-title">Ajuda</h3><!-- /.menu-title -->
                <li>
                    <a href="#"> <i class="menu-icon ti-email"></i>Abrir Chamado</a>
                </li>
                <li>
                    <a href="<?= base_url()?>enviaemailsuporte"> <i class="menu-icon ti-email"></i>Enviar E-mail</a>
                </li>
                <li>
                    <a href="#"> <i class="menu-icon ti-email"></i>Duvidas Frequêntes</a>
                </li>
                <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                        <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>