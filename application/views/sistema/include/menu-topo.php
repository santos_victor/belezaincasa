<div id="right-panel" class="right-panel">
    <header id="header" class="header">
        <div class="header-menu">
            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <div class="header-left">
                    <button class="search-trigger"><i class="fa fa-search"></i></button>
                    <div class="form-inline">
                        <form class="search-form">
                            <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                            <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                        </form>
                    </div>
                    <!-- <div class="dropdown for-notification">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="count bg-danger">5</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">You have 3 Notification</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Server #1 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Server #2 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Server #3 overloaded.</p>
                            </a>
                        </div>
                    </div>-->
                    <?php
                    $contamensagensnaolidas = $this->Mensagem_model->ContMensagensNaoLidas();
                    $mensagens = $this->Mensagem_model->buscaMensagens(usuario('usu_id'));
                    ?>
                    <div class="dropdown for-message">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-email"></i>
                            <span class="count bg-primary"><?php echo $contamensagensnaolidas; ?></span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="message" >
                            <p class="red">Você tem <?php echo $contamensagensnaolidas ?> mensagens</p>
                            <?php foreach ($mensagens as $mensagem): ?>
                                <a class="dropdown-item media bg-flat-color-1" href="<?= base_url()?>respondermensagem/<?=$mensagem['mens_id']?>" >                              
                                    <span class="message media-body">
                                        <span class="name float-left"><?= $mensagem['mens_nome_contato'] ?></span>  
                                        <p><?= substr($mensagem['mens_mensagem_contato'], 0, 10) ?></p>
                                    </span>                                
                                </a>  
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                    
                        <img class="user-avatar rounded-circle" src="<?= base_url() ?>public/images/freelas/<?= usuario('freelancer_foto') ?>">
                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?= base_url() ?>cadastrousuario"><i class="fa fa-user"></i> Meu Perfil</a>
                            <a class="nav-link" href="#"><i class="fa fa-cog"></i> Configurações</a>
                            <a class="nav-link" href="<?= base_url() ?>logout"><i class="fa fa-power-off"></i> Logout</a>
                        </div>
                </div>
                <div class="language-select dropdown" id="language-select">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                        <i class="flag-icon flag-icon-us"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="language">
                        <div class="dropdown-item">
                            <span class="flag-icon flag-icon-fr"></span>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-es"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-us"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-it"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

