<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Site/telaInicial';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['home'] = 'Sistema/home';
//login sistema
$route['autenticarsistema'] = 'login/Login_controller/autenticar';
$route['telalogin'] = 'Sistema/telaLogin';
$route['logout'] = 'login/Login_controller/logout';

//usuario
$route['cadastrousuario'] = 'usuario/Usuario_controller/cadastroUsuario';
$route['cadastrarusuario'] = 'usuario/Usuario_controller/cadastrarUsuario';
$route['criarcontasite'] = 'usuario/Usuario_controller/cadastroUsuarioSite';
$route['cadastrarusuariosite'] = 'usuario/Usuario_controller/cadatrarUsuarioSite';

//anuncios
$route['cadastroanuncio'] = 'anuncio/Anuncio_controller/cadastroAnuncio';
$route['cadastraranuncio'] = 'anuncio/Anuncio_controller/cadastrarAnuncio';
$route['anunciolista/(:any)'] = 'anuncio/Anuncio_controller/listaAnuncio/$1';
$route['anuncioaltera/(:any)'] = 'anuncio/Anuncio_controller/alteraAnuncio/$1';
$route['excluiranuncio/(:any)'] = 'anuncio/Anuncio_controller/excluirAnuncio/$1';
$route['anunciopagamento'] = 'anuncio/Anuncio_controller/anuncioPagamento';

//freelancer
$route['enviar_mensagem_freelancer/(:any)'] = 'freelancer/Freelancer_controller/mensagemFreelancerRecebe/$1';
$route['perfilfreelancer/(:any)'] = 'usuario/Usuario_controller/perfilFrellancerSite/$1';
$route['avaliarfreelancer/(:any)'] = 'freelancer/Freelancer_controller/avaliarFreelancer/$1';
$route['mensagensrecebidas'] = 'freelancer/Freelancer_controller/mensagensRecebidas';
$route['respondermensagem/(:num)'] = 'freelancer/Freelancer_controller/responderMensagem/$1';
$route['respondemensagemcliente'] = 'freelancer/Freelancer_controller/respondeMensagem';
$route['mensagensrespondidas'] = 'freelancer/Freelancer_controller/mensagensRespondidas';
$route['excluirmensagem/(:any)'] = 'freelancer/Freelancer_controller/excluirMensagem/$1';
$route['buscarfreelancer'] = 'freelancer/Freelancer_controller/buscarFreelancer';
$route['enviaemailsuporte'] = 'freelancer/Freelancer_controller/enviaEmailSuporte';
$route['enviaremailsuporte'] = 'freelancer/Freelancer_controller/enviarEmailSuporte';


//layout site
$route['cadastrese'] = 'Site/infoCadastroFreelancer';

