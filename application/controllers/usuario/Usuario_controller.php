<?php

defined('BASEPATH') OR exit('No direct script access allowe');

class Usuario_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Usuario_model');
        $this->load->model('Anuncio_model');
        $this->load->model('Freelancer_model');
        $this->load->model('Estados_model');
        //logado();
    }

    //Tela de cadastro completo de usuário
    public function cadastroUsuario() {
        $usuario = usuario('usu_id');
        $dados = [
            'tela' => 'sistema/usuario/cadastrousu_view'
        ];
        $dados['cidades'] = $this->Estados_model->selectCidadesBrasilia();
        $dados['usuario'] = $this->Usuario_model->buscaUsuario($usuario);
        $dados['categorias'] = $this->Freelancer_model->selectCategoria();
        //debug($dados);
        $this->load->view('principal_in', $dados);
    }

    //Cadastro completo de usuário, cadastra dados na tabela freelancer
    public function cadastrarUsuario() {

        $foto = $_FILES['myfile'];

        if (!empty($foto["name"])) {

            $config['upload_path'] = './public/images/freelas';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 3000;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('myfile')) {
                $error = array('error' => $this->upload->display_errors());

                debug($error);
            }
        }

        $dados = array(
            'id' => $_POST['id'],
            'freelancer_nome' => $_POST['nome'],
            'freelancer_area' => $_POST['area'],
            'freelancer_especial' => $_POST['especial'],
            'freelancer_telefone' => $_POST['telefone'],
            'freelancer_celular' => $_POST['celular'],
            'freelancer_insta' => $_POST['insta'],
            'freelancer_endereco' => $_POST['endereco'],
            'freelancer_bairro' => $_POST['bairro'],
            'freelancer_cidade' => $_POST['cidade'],
            'freelancer_estado' => $_POST['estado'],
            'freelancer_cep' => $_POST['cep'],
            'freelancer_hora' => date('H:i:s'),
            'freelancer_data' => date('Y-m-d'),            
        );

        if (!empty($foto['name'])):
            $dados['freelancer_foto'] = $foto['name'];
        endif;

        $this->Usuario_model->insereFreelancer($dados);
        
        if (!empty($_POST['senha'])) {
            $dados = array(
                "usu_id" => usuario('usu_id'),
                "usu_senha" => md5($_POST['senha'])
            );
            $this->Usuario_model->alteraSenhaUsuario($dados);
            $dados['usu_senha'] = md5($_POST['senha']);
        }        
        $this->session->set_flashdata("success", "Cadastrado com sucesso!");
        redirect('cadastrousuario');
    }

    //Tela de cadastro de usuário, primeira etapa de cadastro no site
    public function cadastroUsuarioSite() {
        $dados = array(
            'tela' => 'sistema/usuario/cadastrousuariosite_view'
        );
        $this->load->view("login_sistema", $dados);
    }

    public function cadatrarUsuarioSite() {
        $user = array(
            'usu_usuario' => $_POST['email'],
            'usu_senha' => md5($_POST['senha']),
            'usu_hora' => date('H:i:s'),
            'usu_data' => date('Y-m-d'),
        );
        $user_id = $this->Usuario_model->insereUsuario($user);
        
        $dados = array(
            'freelancer_nome' => $_POST['nome'],
            'freelancer_usuid' => $user_id,
        );
        //debug($dados);
        $this->Usuario_model->insereFreelancerSite($dados);

        $this->session->set_flashdata("success", "Cadastrado com sucesso!");
        redirect('telalogin');
    }
    
    public function perfilFrellancerSite($id){
        $dados = array(
        'tela' => 'site/freelancer/perfil_freelancer'    
        );
        $dados['freelancer'] = $this->Anuncio_model->selecionarAnuncioFreelancer($id);
        //debug($dados['freelancer']);
        $this->load->view('principalSite', $dados);
    }

}
