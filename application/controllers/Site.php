<?php

defined('BASEPATH') OR exit('No direct script access allowe');

class Site extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Anuncio_model');
        $this->load->model('Avaliacao_model');
        $this->load->model('Freelancer_model');
        $this->load->model('Estados_model');
        
        //logado();

    }

    public function telaInicial() {
        $dados = array(
            'tela' => 'site/principal/principal_view'
        );
        $dados['anuncios'] = $this->Anuncio_model->selecionarTodosOsAnuncios();
        $dados['avaliacoes'] = $this->Avaliacao_model->melhoresAvaliacoes();
        $dados['categorias'] = $this->Freelancer_model->selectCategoria();
        $dados['estados'] = $this->Estados_model->selectEstados();
        $dados['brasiliacidades'] = $this->Estados_model->selectCidadesBrasilia();
        //debug($dados['avaliacoes']);
        $this->load->view('principalSite', $dados);

    }

public function infoCadastroFreelancer(){
    $dados = array(
        'tela' => 'site/layout/infocadastro_view'
    );
    $this->load->view('principalSite',$dados);
}

}

