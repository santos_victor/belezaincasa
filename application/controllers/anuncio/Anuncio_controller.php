<?php

defined('BASEPATH') OR exit('No direct script access allowe');

class Anuncio_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Usuario_model');
        $this->load->model('Anuncio_model');
        //logado();
    }

    public function cadastroAnuncio() {
        //$id_usuario = usuario('usu_id');
        //$usuario = $this->Usuario_model->buscaUsuario($id_usuario);
        //debug($usuario['anuncio_publicado']);
        if (!isset($_GET['id'])) {
            $dados = array(
                'tela' => 'sistema/anuncio/anuncio_view'
            );
            $this->load->view('principal_in', $dados);
        } else {
            $id = $_GET['id'];
            $dados = array(
                'tela' => 'sistema/anuncio/anuncio_view'
            );
            $dados['anuncio'] = $this->Anuncio_model->selecionarAnuncioAlterar($id);
            //debug($dados);
            $this->load->view('principal_in', $dados);
        }
    }

    public function cadastrarAnuncio() {
        $id_usuario = usuario('freelancer_id');
        $usuario_anuncio = $this->Usuario_model->buscaUsuario($id_usuario);
        if ($usuario_anuncio['anuncio_id_freela'] == "") {
            $anuncio = array(
                'anuncio_titulo' => $_POST['titulo'],
                'anuncio_desc' => $_POST['descricao'],
                'anuncio_id_freela' => usuario('freelancer_id'),
                'anuncio_hora' => date('H:i:s'),
                'anuncio_data' => date('Y-m-d'),
                'anuncio_status' => 0
            );
            $this->Anuncio_model->insereAnuncio($anuncio);
            $this->session->set_flashdata("success", "Anúncio Cadastrado com sucesso!");
            redirect('cadastroanuncio');
        } else {
            $this->session->set_flashdata("danger", "Você já cadastrou um anúncio!");
            redirect('cadastroanuncio');
        }
    }

    public function listaAnuncio($id) {
        $dados = array(
            'tela' => 'sistema/anuncio/anunciolista_view'
        );
        $dados['anuncios'] = $this->Anuncio_model->selecionarAnuncio($id);
        //debug($dados['anuncios']);
        $this->load->view('principal_in', $dados);
    }

    public function alteraAnuncio($id) {
        $anuncio = array(
            'anuncio_id' => $id,
            'anuncio_titulo' => $_POST['titulo'],
            'anuncio_desc' => $_POST['descricao'],
            'anuncio_dt_alteracao' => date('Y-m-d'),
            'anuncio_hora_alteracao' => date('H:i:s'),
        );

        if ($this->Anuncio_model->updateAnuncio($anuncio)) {
            $this->session->set_flashdata("success", "Alterado com sucesso!");
            redirect('cadastroanuncio?id=' . $id);
        } else {
            $this->session->set_flashdata("danger", "Erro ao salvar");
            redirect('cadastroanuncio?id=' . $id);
        }
    }

    public function excluirAnuncio($id) {
        $idusuario = usuario('freelancer_id');
        //debug($idusuario);
        $exluiranuncio = $this->Anuncio_model->exluirAnuncio($id);
        if ($exluiranuncio) {
            $this->session->set_flashdata("success", "Exluído com sucesso!");
            redirect('anunciolista/' . $idusuario);
        } else {
            $this->session->set_flashdata("danger", "Não foi possível exluir");
            redirect('anunciolista/' . $idusuario);
        }
    }

    public function anuncioPagamento() {
        $anuncio = $this->Anuncio_model->selecionarAnuncioFreelancer(usuario('freelancer_id'));
        //debug($anuncio['anuncio_id']);
        $dados['triagem'] = $this->Anuncio_model->buscaTriagem($anuncio['anuncio_id']);
        if ($dados['triagem'] != "" ) {
//        $usuario_id = usuario("freelancer_id");
//        $valor = "29.99";
//        $referencia = rand(1, 9999);
//        $forma = 'Mercado Pago';
//        $status = 'Pendente';
//
//        $valores = array(
//            'fatura_usu_id' => $usuario_id,
//            'fatura_ref' => $referencia,
//            'fatura_forma' => $forma,
//            'fatura_data' => date('Y-m-d'),
//            'fatura_valor' => $valor,
//            'fatura_status' => $status,
//        ); 
//        $dados['fatura'] = $this->Anuncio_model->insereFatura($valores);        
        //debug($dados['fatura']);
            $dados = array(
                'tela' => 'sistema/anuncio/anunciopagamento_view'
            );
            $dados['fatura'] = $this->Anuncio_model->buscaFatura(usuario('freelancer_id'));
            $this->load->view('principal_in', $dados);
        } else {
            $dados = array(
                'tela' => 'sistema/anuncio/anunciopagamento_view'
            );
            $dados['fatura'] = array(
                'titulo' => "Prezado(a)". usuario('freelancer_nome'),
                'mensagem' => "Vimos que ainda não passou por nosso mês gratuíto de avaiação"
            );
            $this->load->view('principal_in', $dados);
        }
    }

}
