<?php
defined('BASEPATH') OR exit('No direct script access allowe');

class Login_controller extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model("Usuario_model");
        //logado();
    }
    
    public function autenticar(){
        $login = $this->input->post('login');
        $senha = md5($this->input->post('senha'));
        $usuario = $this->Usuario_model->selectUsuario($login, $senha);
        if($usuario){
           unset($usuario['usu_senha']);
           $usuario['usuario_logado'] = 1;
           iniciar_sessao($usuario);
           $this->session->set_flashdata("success", "Logado com sucesso");
           //debug(usuario());
           redirect('home');
          
       }else{
           $this->session->set_flashdata("danger", "Usuário ou senha inválida");
           redirect('telalogin');
       }
        
    }
    
    public function logout(){
        finalizar_sessao();
        redirect('telalogin');
    }
}