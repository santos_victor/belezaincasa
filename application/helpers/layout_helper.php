<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Upload de img com preview
if (!function_exists('upImg')) {

    if (!function_exists('upImg')) {

    function upImg($campo='imagem',$src='',$titulo='',$file='myfile',$arquivo='') {
        echo " <label for='file'>Imagem $titulo</label>
                <input id='file' name='$file' type='file' class='form-control' value='$arquivo'/><br>
                <input id='img' type='hidden' name='$campo' value='$arquivo' />
                <center>
                    <img id='preview' src='$src' class='img-fluid' alt='Prévia da imagem' style='border-radius:10px; width: 200px;' />
                </center> ";
    }
}

if(!function_exists('admin')){
    
    function admin($foto, $logout='login/Login_controller/logout'){
        echo "<div class='col-sm-5'>
                <div class='user-area dropdown float-right'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        <img class='user-avatar rounded-circle' src='$foto' alt='User Avatar'>
                    </a>

                    <div class='user-menu dropdown-menu'>
                        <a class='nav-link' href='#'><i class='fa fa- user'></i>My Profile</a>

                        <a class='nav-link' href='#'><i class='fa fa- user'></i>Notifications <span class='count'>13</span></a>

                        <a class='nav-link' href='#'><i class='fa fa -cog'></i>Settings</a>

                        <a class='nav-link' href='".base_url().$logout."'><i class='fa fa-power -off'></i>Logout</a>
                    </div>
                </div>

                <div class='language-select dropdown' id='language-select'>
                    <a class='dropdown-toggle' href='#' data-toggle='dropdown'  id='language' aria-haspopup='true' aria-expanded='true'>
                        <i class='flag-icon flag-icon-us'></i>
                    </a>
                    <div class='dropdown-menu' aria-labelledby='language' >
                        <div class='dropdown-item'>
                            <span class='flag-icon flag-icon-fr'></span>
                        </div>
                        <div class='dropdown-item'>
                            <i class='flag-icon flag-icon-es'></i>
                        </div>
                        <div class='dropdown-item'>
                            <i class='flag-icon flag-icon-us'></i>
                        </div>
                        <div class='dropdown-item'>
                            <i class='flag-icon flag-icon-it'></i>
                        </div>
                    </div>
                </div>

            </div>";
    }
}
}


