<?php

if (!function_exists('iniciar_sessao')) {

    function iniciar_sessao($usuario) {
        $ci = & get_instance();
        $ci->session->set_userdata($usuario);
    }

    if (!function_exists('usuario')) {

        function usuario($field = '', $value = '', $clear = false) {

            $ci = & get_instance();

            if ($value != '' || $clear == true):
                $ci->session->set_userdata($field, $value);
            endif;

            if ($field != ''):
                return $ci->session->userdata($field);
            else:
                return $ci->session->all_userdata();
            endif;
        }

    }

    if (!function_exists('finalizar_sessao')) {

        function finalizar_sessao() {
            $ci = & get_instance();
            $ci->session->sess_destroy();
        }

    }

    if (!function_exists('logado')) {

        function logado() {
            $ci = & get_instance();
            $ok = $ci->session->userdata('usuario_logado');
            if (!empty($ok)):
                $ussu = usuario();
                if (!isset($ussu['usu_id'])):
                    finalizar_sessao();
                    redirect('homesistema');
                endif;
            endif;
            if (empty($ok)):
                redirect('login');
            endif;
        }

    }

    if (!function_exists('formataDate')):

        function formataDate($date, $separator) {
            if ($date != '') {
                $date = explode($separator, $date);
                if ($separator == "-")
                    $separator02 = "/";
                else
                    $separator02 = "-";
                $date2 = $date[2] . $separator02 . $date[1] . $separator02 . $date[0];
                return $date2;
            }
        }

    endif;
}